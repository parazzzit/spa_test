<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use  Socialite;
use App\post;
//use Illuminate\Support\Facades\Auth;
use App\Services\SocialFacebookAccountService;


class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service, Request $request)
    {
        if (parse_url($request->session()->get('_previous')['url'])['path'] != '/redirect') {
            return redirect()->route('/login');
        }
        $user = Socialite::driver('facebook')->user();
        $aut = $service->createOrGetUser($user);
        $this->checkPosts($user->getId(), $user->token);
        auth()->login($aut);

        $token = $user->token;
        $data = array(
            'uid' => 'My App',
            'Description' => 'This is New Application',
            'author' => 'foo'
        );

        return redirect()->to("/user")->with(['token' => $token, 'uid' => $user->getId()]);
    }

    private function checkPosts($uid, $token)
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', "https://graph.facebook.com/v3.0/me/posts?fields=message%2Ccaption%2Ccreated_time%2Ctype%2Cattachments%2Cdescription%2Cname%2Ctarget&access_token={$token}");
        $response = \GuzzleHttp\json_decode($res->getBody());

        foreach ($response->data as $data) {
            if (!post::where('fb_postid', '=', $data->id)->exists())
                post::create(['uid' => $uid, 'fb_postid' => $data->id, 'data' => json_encode($data), 'created_time' => strtotime($data->created_time)]);
        }
    }
}
