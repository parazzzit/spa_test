<?php

namespace App\Http\Controllers;

use App\SocialFacebookAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function index() {
        $id = SocialFacebookAccount::whereUserId(Auth::id())->first(['provider_user_id']);

        return view('admin.posts.index', ['uid' => $id->provider_user_id]);
    }
}
