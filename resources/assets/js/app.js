
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
//
// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./bootstrap');
window.Vue = require('vue');
import VueRouter from 'vue-router';


window.Vue.use(VueRouter);

import PostsIndex from './components/Posts/PostsIndex.vue';
this.authuser = authuser;

Vue.component('show-post', {
    data: function () {
        return {
        }
    },
    props: ['content'],
    template: "<div>" +
    "   <span class=\"content.type\"><b>{{content.message}}</b></span>" +
    "   <p><div v-if=\"content.attachments\">" +

    "       <div v-for='attach in content.attachments.data'>" +
    "               <p><span>{{attach.title}}</span></p>" +
    "           <div v-if='attach.subattachments'>" +
    "               <div v-for='subattach in attach.subattachments.data'>" +
    "                   <p><span>{{subattach.title}}</span></p>" +
    "                   <p><img width='720' height='405' v-if=\"'media' in subattach\" :src=\"subattach.media.image.src\"/></p>" +
    "               </div>" +
    "           </div>" +
    "           <div v-else>" +
    "               <p><img :width=\"attach.media.image.width\" :height=\"attach.media.image.height\" v-if=\"'media' in attach\" :src=\"attach.media.image.src\"/></p>" +
    "           </div>" +
    "       </div>" +
    "   </div></p>" +
    "   <p>{{content.description}}</p>" +
    "</div>",

})

const PostsEdit = {
    template:
    "<div style='word-break: break-word;'>" +
    "<div class=\"panel panel-default\">\n" +
    "   <div class=\"panel-heading\" style=\"display: flow-root;\">" +
    "       <div class=\"panel-title pull-left\">\n" +
    "           <h4>{{post.data | checkvar('caption', 'type')}}</h4> " +
    "           <h6 style=\"margin: 0 0 0 0; font-weight: 600; color: #607D8B;\">{{ post.data | checkvar('name') }} <span v-if=\"post.data.name && post.data.target\" class=\"glyphicon glyphicon-chevron-right\" style=\"font-size: 9px; top: 0px; color: dimgray;\"></span>\n" +
    "                                        <span>&nbsp{{ post.data | checkvar('target').name }} </span></h6>" +
    "           <p class=\"under-caption\" style='font-size: 12px; color: dimgray;'>{{post.data.created_time | parseDate}}</p>" +
    "       </div>\n" +
    "        " +
    "       <div class=\"panel-title pull-right\">" +
    "           <router-link  :to=\"{ name: 'postsIndex', params: { id: id }}\"  ><span class='glyphicon glyphicon-remove'></span></router-link>\n" +
    "       </div>\n" +
    "   </div>\n" +
    "      <div class=\"panel-body\">" +
    " <show-post :content='post.data'></show-post> </div>" +
    "</div>" +
    "</div>",
       methods : {

        getPost(){
            var app = this;
               axios.get('/api/v1/user/'+app.id+'/posts/' + app.pid)
                   .then(function (resp) {
                       app.post = resp.data[0];
                       app.post.data = JSON.parse(app.post.data);
                   })
                   .catch(function () {
                       alert("Could not load your post")
                   });
        }
    },
    mounted() {
        console.log('asd');
        var app = this;
        app.id = 0;
        app.pid = 0;
        app.id = app.$route.params.id;
        app.pid = app.$route.params.pid;
        this.getPost();
    },

    watch: {
        gpost: function(n,o) {
            var app = this;
            app.id = app.$route.params.id;
            app.pid = app.$route.params.pid;
            this.getPost();
        }
    },
    data : function() {
        return {
            post: {},
            id: '',
        }
    },
    props: [
      'gpost'
    ],

    filters: {
        checkvar: function(value,arg1, arg2, arg3){
            return (value != undefined) ? ((arg1 in value) ? value[arg1] : ((arg2 in value) ? value[arg2] : ((arg3 in value) ? value[arg3] : ''))) : '';
        },

        parseDate: function(value) {
            var d = new Date(value);
            return d.toLocaleDateString() + ' '+ d.toLocaleTimeString();
        },
    },


}

const routes = [
    {path: '/', redirect: '/'+this.authuser+'/posts', component: PostsIndex, name: 'postsIndex'},
    {path: '/:id/posts', component: PostsIndex, name: 'postsIndex', children: [ { path: '/:id/posts/:pid', component: PostsEdit, name: 'showPost' } ] },
]

const router = new VueRouter({ routes })

const app = new Vue({ router,}).$mount('#app')
