@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
                    <div class="panel-heading">Posts</div>

                    <div class="panel-body table-responsive">
                         <router-view></router-view>
            </div>
        </div>
    </div>
@endsection